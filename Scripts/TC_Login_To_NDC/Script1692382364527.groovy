import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('org.ndc')

Mobile.verifyElementText(findTestObject('Object Repository/for_Login/text_Welcome to NDC Apps'), 'Welcome to NDC Apps')

WebUI.verifyElementText(findTestObject('for_Login/text_ Login or Sign up to continue'), 'Login or Sing up to continue')

WebUI.verifyElementText(findTestObject('for_Booking_Seat/text_Enter your phone number'), 'Enter your phone number')

Mobile.setText(findTestObject('Object Repository/for_Login/textbox_Phone Number'), '0', 1)

Mobile.tap(findTestObject('for_Login/button_Login'), 0)

Mobile.verifyElementText(findTestObject('for_Login/text_Password Verification'), 'Password Verification')

String text2 = Mobile.getText(findTestObject('for_Login/text_ Enter your password that registered with the account'), 0)

Mobile.verifyElementText(findTestObject('for_Login/text_Forget Password'), 'Forget Password?')

Mobile.verifyElementText(findTestObject('for_Login/text_Reset Password'), 'Reset Password')

Mobile.setEncryptedText(findTestObject('for_Login/textbox_Password'), 'lN6pIkJRK06vQnptMhx1ew==', 0)

Mobile.tap(findTestObject('for_Login/button_Password_Verification'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('for_Login/text_Welcome,'), 'Welcome')

Mobile.takeScreenshot('C:\\Users\\user\\Pictures\\Saved Pictures', FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

