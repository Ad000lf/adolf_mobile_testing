import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('for_Booking_Seat/menu_SeatBooking'), 0)

Mobile.scrollToText('Ibadah Raya', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('for_Booking_Seat/schedule__Monday_Cp'), 0)

Mobile.tap(findTestObject('for_Login/button_0900'), 0)

Mobile.scrollToText('Lanjutkan', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/button_Lanjutkan(In_Seat_Booking_Page)'), 0)

Mobile.tap(findTestObject('for_Booking_Seat/button_Lanjutkan(In_Seat_Booking_Page)'), 0)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/Text_Mohon membaca ketentuan di bawah ini sebelum melanjutkan'), 
    'Mohon membaca ketentuan di bawah ini sebelum melanjutkan\' or . = \'Mohon membaca ketentuan di bawah ini sebelum melanjutkan')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_1. Data yang saya masuk'), 'Mohon membaca ketentuan di bawah ini sebelum melanjutkan\' or . = \'Mohon membaca ketentuan di bawah ini sebelum melanjutkan')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_2. Saya mengetahui,'), 'Saya mengetahui, memahami, dan bersedia membebaskan NDC dan semua pihak dalam tuntutan apapun jika terjadi hal-hal yang tidak diinginkan. Dan saya bersedia menerima resiko serta konsekuensi yang muncul di kemudian hari, secara pribadi.\' or . = \'2. Saya mengetahui, memahami, dan bersedia membebaskan NDC dan semua pihak dalam tuntutan apapun jika terjadi hal-hal yang tidak diinginkan. Dan saya bersedia menerima resiko serta konsekuensi yang muncul di kemudian hari, secara pribadi.')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_3. e-Ticket tidak berlaku 10 menit sejak ibadah dimulai'), 
    '3. e-Ticket tidak berlaku 10 menit sejak ibadah dimulai.\' or . = \'3. e-Ticket tidak berlaku 10 menit sejak ibadah dimulai.')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_4. Pintu akan ditutup'), '4. Pintu akan ditutup 30 menit sejak ibadah dimulai. Anda dapat mengikuti ibadah selanjutnya apabila terlambat')

Mobile.checkElement(findTestObject('for_Login/checkbox_Saya_Setuju'), 1)

Mobile.verifyElementVisible(findTestObject('for_Login/button_Setuju(DisclaimerPage)'), 1)

Mobile.tap(findTestObject('for_Login/button_Setuju(DisclaimerPage)'), 1)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Konfirmasi_Booking'), 'Konfirmasi Booking')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Anda_akan_mendftartkan_diri_Anda_untuk'), 'Anda akan mendaftarkan diri Anda untuk')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Ibadah 1 NCH'), 'Ibadah 1 (NCH)')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_0900WIB'), 'Pk. 09:00 WIB')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Booking Ibadah Anak'), 'Booking Ibadah Anak')

Mobile.verifyElementVisible(findTestObject('for_Login/button_Daftar'), 1)

Mobile.tap(findTestObject('for_Login/button_Daftar'), 1)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Pendaftaran_Berhasil'), 'Pendaftaran Berhasil')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Mohon siapkan Aplikasi NDC untuk registrasi'), 'Mohon siapkan Aplikasi NDC untuk registrasi ulang saat masuk ke lokasi ibadah.\' or . = \'Mohon siapkan Aplikasi NDC untuk registrasi ulang saat masuk ke lokasi ibadah.')

Mobile.verifyElementVisible(findTestObject('for_Login/button_Kembali'), 1)

Mobile.tap(findTestObject('for_Login/button_Kembali'), 1)

Mobile.scrollToText('E-ticket', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/icon_E_TIcket'), 1)

