import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('for_Booking_Seat/menu_SeatBooking'), 0)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Seat Booking'), 'Seat Booking')

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/button_Protokol Kesehatan Ibadah On-Site'), 0)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_1'), 'Tanpa seat booking, jemaat tidak dapat mengikuti ibadah onsite, dan dianjurkan ibadah melalui:\' or . = \'Tanpa seat booking, jemaat tidak dapat mengikuti ibadah onsite, dan dianjurkan ibadah melalui')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_YouTube Channel NDC'), '• YouTube Channel NDC')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text -Website ndcministry.tv'), '• Website ndcministry.tv')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/Text_NDC Youth'), 'NDC Youth')

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/schedule_for_NDC_Youth_CP_Saturday'), 1)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/schedule_for_NDC_Youth_CP_Sunday'), 1)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Ibadah_Raya_Minggu'), 'Ibadah Raya Minggu')

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/schedule__Monday_Cp'), 1)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/schedule_Monday_Living_Word'), 1)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/scheduler_Monday_Baywalk'), 1)

Mobile.verifyElementVisible(findTestObject('for_Booking_Seat/button_Lanjutkan(In_Seat_Booking_Page)'), 0)

Mobile.takeScreenshot('C:\\Users\\user\\Pictures\\Saved Pictures', FailureHandling.STOP_ON_FAILURE)

