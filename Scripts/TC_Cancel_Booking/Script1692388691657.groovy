import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('for_Booking_Seat/icon_E_TIcket'), 1)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_E-Ticket'), 'E-Ticket')

WebUI.verifyElementVisible(findTestObject('for_Login/icon_Ticket'))

Mobile.scrollToText('Gunakan E-Ticket', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/button_Gunakan_E-Ticket'), 1)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/button_Batalkan_E-Ticket'), 1)

Mobile.tap(findTestObject('for_Cancel_Booking/button_Batalkan_E-Ticket'), 1)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Ingin mengganti jadwal ibadah'), 'Ingin mengganti jadwal ibadah')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Berhalangan untuk hadir ibadah'), 'Berhalangan untuk hadir ibadah')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Ingin mendaftar ulang untuk ibadah anak'), 'Ingin mendaftar ulang untuk ibadah anak')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Lainnya'), 'Lainnya')

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/checkbox_Reason1'), 1)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/checkbox_Reason2'), 1)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/checkbox_Reason3'), 1)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/checkbox_Reason4'), 1)

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/button_Batalkan_E-Ticket'), 1)

Mobile.tap(findTestObject('for_Cancel_Booking/button_Batalkan_E-Ticket'), 1)

Mobile.verifyElementText(findTestObject('for_Booking_Seat/text_Booking dibatalkan'), 'Booking dibatalkan')

Mobile.verifyElementText(findTestObject('for_Booking_Seat/TextService Booking berhasil dibatalkan'), 'Service Booking Berhasil dibatalkan')

Mobile.verifyElementVisible(findTestObject('for_Cancel_Booking/button_Kembali(cancel_Booking)'), 1)

Mobile.tap(findTestObject('for_Cancel_Booking/button_Kembali(cancel_Booking)'), 1)

Mobile.verifyElementNotVisible(findTestObject('for_Booking_Seat/icon_E_TIcket'), 5)

